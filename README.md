# Фреймворк для api и ui тестов

### В работе используется:
* [Python](https://www.python.org/)
* [Pytest](https://docs.pytest.org/)
* [Allure](https://docs.qameta.io/allure/)
* [Mountebank](http://www.mbtest.org/)
* Драйвер [хрома](https://chromedriver.chromium.org/downloads) или [фаерфокса](https://github.com/mozilla/geckodriver/releases) для ui тестов


### Установка
```bash
git clone https://gitlab.com/Murrengan/murr_testing_framework.git
pip install -r requirements.txt
# Если требуются моки:
npm install mountebank 
mb

Положить скачанный драйвер в папку с виртуальны окружением питона venv/bin
```

### Примеры параметров при запуске:
* --hub http://ruapm.online ```url стенда```
* --alluredir=./src/allure_dir ```путь куда алюр пишет данные по тестам```
* --ssid 1674c24b-e73d-4e6f-a14c-5bbdcb437e26 ```токен клиента```
* --data_manager_dataset_dir path_to_framework/src/datasets ```путь к папке с датасетом```

Дополнительные параметры смотри в ```src/conftest.py```

### Что предстоит сделать
* BaseValidator - валидатор ответа от сервера
* ActionsProcessor - автогенератор данных ```пример тут - src/api/tests/purchase/dataset.yml```
* reporter - сделать аллюр фикстурой для удобного переиспользования
* dataset_provider_setter - прокидывать созданные тестовые данные для валидиции
* прописать аннотации типов