import time


class TestArchiveStreamProcessingAtSpeed4:
    settings = 'settings.yml'

    def test(self, yml_settings, dataset_provider, imposter, api_tester):
        for step_setting in yml_settings['steps']:
            if 'sleep' in step_setting:
                time.sleep(step_setting['sleep'])
            api_tester(settings=step_setting)
