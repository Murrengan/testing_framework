import json

import requests


class BaseApiTester:

    def __init__(self, hub, validator, ssid=''):
        self.session = requests.Session()
        self.hub = hub
        self.validator = validator
        self._headers = {'ssid': ssid} if ssid else {}

    def register(self, registration_url, body):
        response = self.session.post(registration_url, data=body)
        if response.status_code != 201:
            raise Exception(f'Failed to register user. registration_url = {registration_url}, body = {body}')

    def authorize(self, login_url, body):
        response = self.session.post(login_url, data=body)
        if response.status_code != 200:
            raise Exception(f'Failed to authorize user. login_url = {login_url}, body = {body}')
        self._headers['ssid'] = json.loads(response.text)['result']['ssid']

    def get_response(self, settings=None):
        if 'url' not in settings:
            raise Exception('No url specified')
        if 'method' not in settings:
            settings['method'] = 'get'
        if 'data' not in settings:
            settings['data'] = None
        response = self.send_request(settings['url'], settings['method'], settings['data'])
        return response

    def send_request(self, url, method, data=None):
        path = f'{self.hub}/{url}'
        request_methods = {
            'get': self.session.get,
            'post': self.session.post,
            'patch': self.session.patch,
            'delete': self.session.delete,
        }
        extra_args = {
            'headers': self._headers
        }
        if data:
            extra_args['data'] = data

        response = request_methods[method.lower()](path, **extra_args)
        return response
