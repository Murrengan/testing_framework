class TestMonitoringLists:
    settings = 'settings.yml'

    def test(self, yml_settings, dataset_provider, imposter, api_tester):
        data = None
        if 'dataset' in yml_settings:
            data = dataset_provider.run(yml_settings['dataset'])
        if 'imposter_data' in yml_settings:
            imposter.run(yml_settings['imposter_data'])
        if 'patch_monitoring_list' in yml_settings:
            yml_settings['url'] += f"/{data['monitoring_list_id']}"
        api_tester(settings=yml_settings)
