import json

import pytest
from faker import Faker

from src.api.base_api_tester import BaseApiTester
from src.validators.base_validator import BaseValidator

fake = Faker()


@pytest.fixture(scope='function')
def base_api_tester(request):
    validator = BaseValidator()
    tester = BaseApiTester(request.config.hub, validator, request.config.ssid)
    return tester


@pytest.fixture(scope='function')
def api_tester(request, base_api_tester):
    def callback(settings=None):
        # TODO attach reporter
        if settings.get('need_auth'):
            registration_url, login_url, body = get_credentials(request)
            base_api_tester.register(registration_url, body)
            base_api_tester.authorize(login_url, body)

        response = base_api_tester.get_response(settings)
        if 'response_status_code' in settings:
            assert response.status_code == settings['response_status_code']

        response_json = response.json()
        if 'result' in settings:
            base_api_tester.validator.validate(response_json['result'], settings['result'])
        return response.json()

    yield callback


@pytest.fixture(scope='function')
def get_credentials(request):
    if request.config.login is None:
        request.config.login = fake.first_name()
    if request.config.password is None:
        request.config.password = fake.password(length=12)
    cred = {"user": {"login": request.config.login, "password": request.config.password}}
    body = json.dumps(cred)
    registration_url = f'{request.config.hub}api/v1/user/registration'
    login_url = f'{request.config.hub}api/v1/user/login'
    return registration_url, login_url, body, cred
