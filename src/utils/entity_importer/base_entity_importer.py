class BaseEntityImporter:

    def __init__(self, backend_api):
        self.backend_api = backend_api

    def create(self, **args):
        pass

    def get(self, **args):
        pass

    def copy(self, **args):
        pass

    def delete(self, **args):
        pass
