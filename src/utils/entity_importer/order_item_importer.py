from src.utils.entity_importer.base_entity_importer import BaseEntityImporter


class OrderItemImporter(BaseEntityImporter):

    def create(self, data):
        orders = [self.backend_api.order_item.create(**data)]
        return orders
