from src.utils.entity_importer.client_importer import ClientImporter
from src.utils.entity_importer.order_importer import OrderImporter
from src.utils.entity_importer.order_item_importer import OrderItemImporter


class EntityImporter:
    def __init__(self, backend):
        self.backend = backend

    def run(self, data_item):
        entity = data_item['entity']
        entity_db_importer = self.get_entity_db_importer(entity)
        if data_item.get('method') is None:
            return entity_db_importer.create(data_item)

    def get_entity_db_importer(self, entity):
        try:
            return {
                'client': ClientImporter(self.backend),
                'order_item': OrderItemImporter(self.backend),
                'order': OrderImporter(self.backend)
            }[entity]
        except KeyError:
            raise Exception(f'Failed to process {entity} entity')
