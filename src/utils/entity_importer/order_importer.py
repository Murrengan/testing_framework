from src.utils.entity_importer.base_entity_importer import BaseEntityImporter


class OrderImporter(BaseEntityImporter):
    def create(self, data):
        orders = [self.backend_api.order.create(**data)]
        return orders
