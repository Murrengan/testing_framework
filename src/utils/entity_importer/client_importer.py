from .base_entity_importer import BaseEntityImporter


class ClientImporter(BaseEntityImporter):

    def create(self, data):
        clients = [self.backend_api.clients.create(**data)]
        return clients
