from requests import Session

from src.utils.entity_api.client_api import ClientsAPI
from src.utils.entity_api.order_api import OrderAPI
from src.utils.entity_api.order_item_api import OrderItemAPI


class BackendApi:
    def __init__(self, hun):
        self.session = Session()
        self.hun = hun
        self.clients = ClientsAPI(self.session, hun)
        self.order = OrderAPI(self.session, hun)
        self.order_item = OrderItemAPI(self.session, hun)

    @staticmethod
    def authorize(login_url, json):
        # TODO implement authorize
        print(login_url, json)
