import json
from copy import deepcopy

import requests

from src.mountebank_template import imposter_template


class ImposterProvider:
    def __init__(self):
        self.port = imposter_template['port']

    def run(self, imposter_data):
        prepared_imposter = self.patcher(imposter_data)
        requests.request('POST',
                         'http://127.0.0.1:2525/imposters/',
                         data=json.dumps(prepared_imposter),
                         headers={"content-type": "application/json"})

    def patcher(self, imposter_data):
        if imposter_data.get('port'):
            self.port = imposter_data['port']
        prepared_imposter = deepcopy(imposter_template)
        for key, value in imposter_data.items():
            if prepared_imposter.get(key):
                prepared_imposter[key] = value
        return prepared_imposter

    def clear_imposter(self):
        requests.request('DELETE', f'http://127.0.0.1:2525/imposters/{self.port}')
