class DataManager:
    def __init__(self, converter, actions_processor, entity_importer):
        self.converter = converter
        self.actions_processor = actions_processor
        self.entity_importer = entity_importer
        self.data_processed = {}

    def run(self, data):
        # Convert data if required.
        if self.converter is not None:
            data = self.converter.run(data)
        # Run template actions
        for data_item in data:
            self.actions_processor.run(data_item)
        # Run data processor.
        if self.entity_importer is not None:
            for data_item in data:
                self.data_processed[data_item['entity']] = self.entity_importer.run(data_item)
