class ActionsProcessor:
    """Run actions in dataset"""

    @staticmethod
    def run(data_item):
        # TODO implement actions like randomize, multiply, depend_on__ and etc
        data_item['actions_processor_pass'] = True
        return data_item
