from src.utils.entity_api.base_api import BaseApi


class ClientsAPI(BaseApi):
    api = 'v1/client/create'

    def create(self, **kwargs):
        response = self._post(self.api, **kwargs)
        if response.status_code == 201:
            item = response.json()
        else:
            raise Exception(f'Failed to create client. {response.text}')
        return item
