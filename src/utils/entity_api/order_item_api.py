from src.utils.entity_api.base_api import BaseApi


class OrderItemAPI(BaseApi):
    api = 'service/v1/order_item/create'

    def create(self, **kwargs):
        response = self._post(self.api, **kwargs)
        if response.status_code == 201:
            item = response.json()
        else:
            raise Exception(f'Failed to create order_item. {response.text}')
        return item
