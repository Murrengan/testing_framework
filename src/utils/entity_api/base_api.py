class BaseApi:

    def __init__(self, session, hub):
        self.session = session
        self.hub = hub

    def get_path(self, api):
        path = '{}/{}'.format(self.hub, api)
        return path

    def _post(self, url, params=None, headers=None, **json):
        callback = self.session.post
        args = {
            'params': params,
            'json': json
        }
        if headers:
            args['headers'] = headers
        return self._send(url, callback, args)

    def _send(self, url, callback, args):
        send_args = dict(**args)
        response = callback(self.get_path(url), **send_args)
        return response
