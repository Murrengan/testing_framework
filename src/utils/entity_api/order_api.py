from src.utils.entity_api.base_api import BaseApi


class OrderAPI(BaseApi):
    api = 'v1/order/create'

    def create(self, **kwargs):
        response = self._post(self.api, **kwargs)
        if response.status_code == 201:
            item = response.json()
        else:
            raise Exception(f'Failed to create order. {response.text}')
        return item
