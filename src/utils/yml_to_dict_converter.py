import os

import yaml


class YmlToDictConverter:

    def __init__(self, source_dir):
        self.source_dir = source_dir

    def run(self, source_name):
        result = []
        if not isinstance(source_name, list):
            source_name = [source_name]
        for name in source_name:
            dataset_path = os.path.join(self.source_dir, name)
            with open(dataset_path, 'r', encoding='utf-8') as stream:
                data = yaml.safe_load(stream)

            result.extend(data)
        return result
