import allure
from allure_commons.types import AttachmentType


class TestCheckUsername:
    settings = 'settings.yml'

    def test(self, webdriver, page_getter, yml_settings):
        with allure.step('Open main page'):
            page_getter.open_main_page()
            allure.attach(webdriver.driver.get_screenshot_as_png(), attachment_type=AttachmentType.PNG)

        with allure.step(f'Username should be in span and present {yml_settings["spans_len"]} times'):
            spans = webdriver.find_elements_by_css_class(page_getter.locators['username'])
            assert len(spans) == yml_settings["spans_len"]
            for name in spans:
                assert name.text == yml_settings['username']
