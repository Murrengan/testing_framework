class PageMain:
    url = 'session/'

    def __init__(self, hub, ssid, webdriver):
        self.hub = hub
        self.ssid = ssid
        self.webdriver = webdriver
        self.locators = {
            'username': '.username'
        }

    def open_main_page(self):
        self.webdriver.get(f'{self.hub}{self.url}{self.ssid}')
