from cssselect import HTMLTranslator


class DriverWrapper:
    def __init__(self, driver):
        self.driver = driver

    def get(self, url):
        self.driver.get(url)

    def find_elements_by_css_class(self, css_class):
        xpath = convert_css_to_xpath(css_class)
        return self.driver.find_elements_by_xpath(xpath)

    def quit(self):
        return self.driver.quit()


def convert_css_to_xpath(css_class):
    try:
        xpath = HTMLTranslator().css_to_xpath(css_class, prefix='//')
        return xpath
    except Exception as error:
        raise Exception(f'Failed to convert {css_class} to xpath. {error}')
