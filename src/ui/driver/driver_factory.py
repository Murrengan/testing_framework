from selenium import webdriver
from selenium.webdriver.firefox.options import Options

from src.ui.driver.driver_wrapper import DriverWrapper


class DriverFactory:

    def __init__(self, browser, hub=None):
        self.browser = browser
        self.hub = hub

    def get(self):
        driver = self.get_driver()
        return DriverWrapper(driver)

    def get_driver(self):
        driver = self.get_local_driver()
        return driver

    def get_local_driver(self):
        if self.browser == 'firefox':
            options = Options()
            driver = webdriver.Firefox(options=options)
        else:
            driver = webdriver.Chrome()
        return driver
