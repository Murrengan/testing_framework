import allure
import pytest
from allure_commons.types import AttachmentType

from src.ui.driver.driver_factory import DriverFactory
from src.ui.page_object.page_main.page_main import PageMain


@pytest.fixture(scope='function')
def webdriver(request):
    builder = DriverFactory(request.config.browser, request.config.hub)
    driver = builder.get()
    return driver


@pytest.yield_fixture(autouse=True)
def tear_down(webdriver):
    yield True
    with allure.step('Last browser state'):
        allure.attach(webdriver.driver.get_screenshot_as_png(), attachment_type=AttachmentType.PNG)
        webdriver.quit()


@pytest.fixture(scope='function')
def page_getter(request, webdriver):
    page = PageMain(request.config.hub, request.config.ssid, webdriver)
    return page
