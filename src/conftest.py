import os
import sys

import pytest
import yaml

from src.utils.actions_processor import ActionsProcessor
from src.utils.backend_api import BackendApi
from src.utils.data_manager import DataManager
from src.utils.entity_importer.entity_importer import EntityImporter
from src.utils.imposter_provider import ImposterProvider
from src.utils.yml_to_dict_converter import YmlToDictConverter


def pytest_addoption(parser):
    parser.addoption('--browser', action='store', default='chrome')
    parser.addoption('--hub', action='store')
    parser.addoption('--ssid', action='store')
    parser.addoption('--login', action='store')
    parser.addoption('--password', action='store')
    parser.addoption('--data_manager_dataset_dir', action='store')


def pytest_configure(config):
    config.browser = config.getoption('browser')
    config.hub = config.getoption('hub')
    config.ssid = config.getoption('ssid')
    config.login = config.getoption('login')
    config.password = config.getoption('password')
    config.data_manager_dataset_dir = config.getoption('data_manager_dataset_dir')


def name(args):
    return args['name']


def pytest_generate_tests(metafunc):
    """Parametrise tests by yml"""
    if 'yml_settings' in metafunc.fixturenames:
        module_dir = os.path.dirname(sys.modules[metafunc.cls.__module__].__file__)
        settings_files = metafunc.cls.settings
        if not isinstance(settings_files, list):
            settings_files = [settings_files]
        settings = []
        for file_name in settings_files:
            file = os.path.join(module_dir, file_name)
            with open(file, 'r', encoding='utf-8') as stream:
                tests = yaml.safe_load(stream)
                for test in tests:
                    settings.append(test)
        metafunc.parametrize('yml_settings', settings, ids=name)


@pytest.fixture(scope='function')
def backend_api(request):
    """
    Authorize superuser for prepare and change stand properties
    """
    backend = BackendApi(request.config.hub)
    json_params = {
        "login": request.config.login,
        "password": request.config.password
    }
    backend.authorize(request.config.hub, json_params)
    return backend


@pytest.fixture(scope='function')
def dataset_provider(request, backend_api):
    dataset_dir = request.fspath.dirname
    if request.config.data_manager_dataset_dir is not None:
        dataset_dir = request.config.data_manager_dataset_dir
    converter = YmlToDictConverter(dataset_dir)
    actions_processor = ActionsProcessor()
    entity_importer = EntityImporter(backend_api)
    data_manager = DataManager(converter, actions_processor, entity_importer)
    return data_manager


@pytest.fixture()
def imposter():
    imposter_provider = ImposterProvider()
    yield imposter_provider
    imposter_provider.clear_imposter()
