imposter_template = {
        "port": 1987,
        "protocol": "http",
        "stubs": [
            {
                "predicates": [
                    {
                        "equals": {
                            "method": "GET",
                            "path": "/api/points"
                        }
                    }
                ],
                "responses": [
                    {
                        "is": {
                            "statusCode": 200,
                            "headers": {"Content-Type": "application/json"},
                            "body": {'data': 'ok'}
                        }
                    }
                ]
            }
        ]
    }
